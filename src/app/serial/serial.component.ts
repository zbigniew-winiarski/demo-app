import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-serial',
  templateUrl: './serial.component.html',
  styleUrls: ['./serial.component.css']
})
export class SerialComponent{
  @Input()
  serial;
}
