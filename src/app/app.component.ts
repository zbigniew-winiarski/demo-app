import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  serials;
  selectedSerial;

  constructor(private dataService:DataService) {}

   ngOnInit() {
    this.dataService.getSerials()
      .subscribe(data => {
        this.serials = data;
        this.selectedSerial = this.serials[0];
      });
  }

  serialSelected(serial) {
    this.selectedSerial = serial;
  }
}
