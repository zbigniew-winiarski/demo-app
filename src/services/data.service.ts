import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class DataService {
  private dataUrl = '../assets/data.json';

  constructor(private http:Http) { }
  //TODO: i could add some serial.ts file which contains model definition  
  getSerials() {
    return this.http.get(this.dataUrl)
      .map(res => {
        return res.json();
      });
  }
}
